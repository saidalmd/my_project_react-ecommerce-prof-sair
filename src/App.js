import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import c1 from './c4.jpg';
import './App.css';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import PageAcceuil from './components/pageAcceuil/PageAcceuil';
import PageVisage from './components/pageVisage/PageVisage';
import PageHair from './components/pageHair/PageHair';
import PageToutVoir from './components/pageToutVoir/PageToutVoir';
import PageCorps from './components/pageCorps/PageCorps';
import PageDetailProduit from './components/pageDetailProduit/PageDetailProduit';
import PagePanier from './components/pagePanier/PagePanier';

import { BrowserRouter,Routes,Route } from 'react-router-dom';



export const PanierContext=React.createContext()

function App() {
  const [panier,setPanier]=useState([])

  useEffect(()=>{
    const listPanier=localStorage.getItem('listPanier');
    if(listPanier){
      setPanier(JSON.parse(listPanier))
    }
    else {
      const initialListpanier=[];
      localStorage.setItem('listPanier',JSON.stringify(initialListpanier))
    }
  },[]);
  return (
    <div className="App">
      <PanierContext.Provider value={{panierVariable:panier,panierMethod:setPanier}}>
      <BrowserRouter>
      <Header/>
      <Routes>
        <Route path='/' element={<PageAcceuil/>}/>
        <Route path='/visage' element={<PageVisage/>}/>
        <Route path='/toutvoir' element={<PageToutVoir/>}/>
        <Route path='/cheveux' element={<PageHair/>}/>
        <Route path='/corps' element={<PageCorps/>}/>
        <Route path='/toutvoir/:id' element={<PageDetailProduit/>}/>
        <Route path='/panier' element={<PagePanier/>}/>
      </Routes>
      <Footer/>
      </BrowserRouter>
      </PanierContext.Provider>
    </div>
  );
}

export default App;
