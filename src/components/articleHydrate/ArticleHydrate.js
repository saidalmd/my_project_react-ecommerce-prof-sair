import React from 'react';
import './ArticleHydrate.css';
import c8 from '../../c13.jpg';

function ArticleHydrate() {
  return (
    <div id='articlehydrate'>
        <section>
            <h3><span>Pour une peau plus </span><span>douce</span> </h3>
            <p>Offrez à votre peau l'amour qu'elle mérite. Les soins de visage et de corps 
                ne sont pas simplement des rituels beauté, mais des expressions d'affection 
                envers vous-même. Chouchoutez votre peau avec des soins délicats, embrassez 
                la fraîcheur de l'éclat naturel.</p>
        </section>
        <section>
            <img src={c8} />
        </section>
    </div>
  )
}

export default ArticleHydrate