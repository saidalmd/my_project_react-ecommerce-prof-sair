import React from 'react';
import './Footer.css';

function Footer() {
  return (
    <footer id="footer">
        <div>
            <h4>Boutique</h4>
            <a href='#'>Tous les produits</a>
            <a href='#'>Top ventes</a>
            <a href='#'>Visage</a>
            <a href='#'>Cheveux</a>
            <a href='#'>Corps</a>
        </div>
        <div>
            <h4>Notre boutique</h4>
            <span>Rue x avenue x </span>
            <span>80000 Agadir, Maroc</span>
            <span>Tel:0608080808</span>
            <span>Email:test@gmail.com</span>
        </div>
        <div>
            <h4>Service client</h4>
            <span>Tel:0608080808</span>
            <span>Email:test@gmail.com</span>
            <span>
                <i className="fa-brands fa-facebook-f"></i>
                <i className="fa-brands fa-instagram"></i>
                <i className="fa-brands fa-x-twitter"></i>
            </span>
            <span>Copyright 2024</span>
        </div>

    </footer>
  )
}

export default Footer