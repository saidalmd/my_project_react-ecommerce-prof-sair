import React,{useState,useEffect} from 'react';
import p1 from '../../p4.webp';
import axios from 'axios';
import './AllProducts.css';
import {useNavigate} from 'react-router-dom';


function AllProducts() {
    const navigate=useNavigate()
    const [produits,setProduits]=useState([])
    // const [detail,setDetail]=useState(false)

    useEffect(()=>{
        axios.get("http://localhost:8001/produits")
            .then(response=>setProduits(response.data))
            .catch(error=>console.log(error))
    },[])
    const styleImage={
        background:`linear-gradient(to top,rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),
        url(${p1})`,
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat", 
        backgroundSize: "cover",
        backgroundAttachment: "scroll"
    }

    const voirDetail=(id)=>{
        // setDetail(true)
        navigate(`/toutvoir/${id}`)
        // axios.get(`http://localhost:8001/produits/${id}`)
        //     .then(response=>setProduits(response.data))
        //     .catch(error=>console.log(error))
    }
    
  return (
    <div >
        <div style={styleImage} id="allproducts">
            <p>Tout voir</p>
        </div>
        <div id="produits">
        {
                   produits.map(element=>{
                    return(
                        <div className='card2' key={element.id} onClick={()=>voirDetail(element.id)}>
                            <img src={element.img}/>
                            <div>
                                <p>{element.title}</p>
                                <p>{element.price} DH</p>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    </div>
  )
        
        }
 

export default AllProducts