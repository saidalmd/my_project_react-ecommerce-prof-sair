import React from 'react';
import TitreVisage from '../titreVisage/TitreVisage';
import ProduitsVisage from '../produitsVisage/ProduitsVisage';


function PageVisage() {
  return (
    <div>
        <TitreVisage/>
        <ProduitsVisage/>
    </div>
  )
}
export default PageVisage