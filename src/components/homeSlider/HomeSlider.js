import React, {useContext} from 'react';
import './HomeSlider.css';
import { ImageContext } from '../pageAcceuil/PageAcceuil';
// import c1 from '../../c1.jpg';


function HomeSlider() {
    const image=useContext(ImageContext)
    const imageBackStyle={
        backgroundImage:`url(${image})`,
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat", 
        backgroundSize: "cover"
    }
  return (
    <div id="homeslider" style={imageBackStyle}>
        {/* <img src={image}/> */}
        {/* <p>Explorez la beauté naturelle Votre destination ultime pour des soins de la peau innovants et des cosmétiques éclatants. </p> */}
    </div>
  )
}

export default HomeSlider