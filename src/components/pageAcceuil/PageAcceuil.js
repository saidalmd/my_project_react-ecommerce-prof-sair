import React from 'react';
import HomeSlider from '../homeSlider/HomeSlider';
import TopVentes from '../topVentes/TopVentes';
import ArticleHydrate from '../articleHydrate/ArticleHydrate';
import Liste from '../liste/Liste';
import c1 from '../../c4.jpg';

export const ImageContext=React.createContext()

function PageAcceuil() {
  return (
    <div>
        <ImageContext.Provider value={c1}>
            <HomeSlider/>
        </ImageContext.Provider>
        <TopVentes/>
        <ArticleHydrate/>
        <Liste/>
    </div>
  )
}

export default PageAcceuil