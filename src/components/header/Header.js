import React from 'react';
import './Header.css';
import { Link } from 'react-router-dom';

function Header() {
  return (
    <header>
        <section>
            <div>
                <i className="fa-brands fa-facebook-f"></i>
                <a href='https://www.instagram.com/'><i className="fa-brands fa-instagram"></i></a>
                <a href='https://twitter.com/?lang=fr'><i className="fa-brands fa-x-twitter"></i></a> 
            </div>
            <div>
                <a href='http://localhost:3000'>BeautyMall</a>
            </div>
            <div>
                <span>Connexion</span>
                <a href='http://localhost:3000/panier'>Panier</a>
            </div>
        </section>
        <nav>
            {/* <Link to="/">Acceuil</Link> */}
            <Link to="/toutvoir">Tout voir</Link>
            {/* <Link to="#">Meilleures ventes</Link> */}
            <Link to="/visage">Visage</Link>
            <Link to="/cheveux">Cheuveux</Link>
            <Link to="/corps">Corps</Link>
            {/* <a href='#'>Tout voir</a>
            <a href='#'>Meilleures ventes</a> */}
            {/* <a href='#'>Visage</a> */}
            {/* <a href='#'>Cheuveux</a>
            <a href='#'>Corps</a> */}
        </nav>
    </header>
  )
}

export default Header