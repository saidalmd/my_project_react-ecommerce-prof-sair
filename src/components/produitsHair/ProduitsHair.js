import React,{useState,useEffect} from 'react';
import axios from 'axios';
import p1 from '../../p2.jpg';
import './ProduitsHair.css';

function ProduitsHair() {
    const [produits,setProduits]=useState([])

    useEffect(()=>{
        axios.get("http://localhost:8001/produits")
            .then(response=>setProduits(response.data.filter(ele=>{
                return ele.type=="cheveux";
            })))
            .catch(error=>console.log(error))
    },[])
    const styleImage={
        background:`linear-gradient(to top,rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),
        url(${p1})`,
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat", 
        backgroundSize: "cover",
        backgroundAttachment: "scroll"
    }
  return (
    <div>
        <div style={styleImage} id="titrehair">
            <p>CHEUVEUX</p>
        </div>
        <div id='produitshair'>
            {
                produits.map(element=>{
                    return(
                        <div className='card2' key={element.id}>
                            <img src={element.img}/>
                            <div>
                                <p>{element.title}</p>
                                <p>{element.price} DH</p>
                            </div>
                        </div>
                    )
                })
            }

        </div>

    </div>
  )
}

export default ProduitsHair