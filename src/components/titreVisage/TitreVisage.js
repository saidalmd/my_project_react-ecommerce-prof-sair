import React from 'react';
import p1 from '../../p2.jpg';
import './TitreVisage.css';


function TitreVisage() {
    const styleImage={
        background:`linear-gradient(to top,rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),
        url(${p1})`,
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat", 
        backgroundSize: "cover",
        backgroundAttachment: "scroll"
    }
  return (
    <div id='titrevisage'>
        <div style={styleImage}>
            <p>VISAGE</p>
        </div>
    </div>
  )
}

export default TitreVisage