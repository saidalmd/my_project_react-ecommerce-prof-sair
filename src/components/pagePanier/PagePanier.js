import React, { useEffect, useState } from 'react';
import './PagePanier.css';

function PagePanier() {
    const [panier,setPanier]=useState([])
    useEffect(()=>{
        const strListPanier=localStorage.getItem('listPanier')
        const listPanier=JSON.parse(strListPanier)
        console.log(listPanier)
        const listComplete=[]

        for(let element of listPanier){
            listComplete.push(JSON.parse(element))
        }
        console.log(listComplete)
        setPanier(listComplete)
        console.log(panier)

    },[])
  return (
    <div id='pagePanier'>
        <div id='monPanier'>
            <p id='title'>Mon panier</p>
            <hr/>
            {
                panier.map(element=>{
                    return(
                    <>
                    <div key={element.id}>
                        <img src={element.img}/>
                        <section id='section1'>
                        <p>{element.title}</p>
                        <p>{element.price} DH</p>
                        </section>
                        
                        <p>{element.price} DH</p>
                        <p>X</p>
                    </div>
                    <hr/>
                    </>
                )})
            }
        </div>
        <div id='resumeCmd'>
            <p id='title'>Résumé de la commande</p>
            <hr/>
            <section id='total'>
            <p>Total</p>
            <p>160 DH</p>
            </section>
            <hr/>
            <p id='p_btn'>Paiement</p>
        </div>
    </div>
  )
}

export default PagePanier