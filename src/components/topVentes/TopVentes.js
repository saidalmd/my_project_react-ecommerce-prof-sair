import React,{useEffect, useState} from 'react';
import './TopVentes.css';
import axios from 'axios';

function TopVentes() {
    const [products,setProducts]=useState([])
    useEffect(()=>{
        axios.get("http://localhost:8001/produits")
            .then(response=>setProducts(response.data.filter(element=>{
                return element.price>120
            }).slice(2,6)))
            .catch(error=>console.log(error))

    },[])
  return (
    <div id="topventes">
        <p>LES INCONTOURNABLES</p>
        <p><span>Meilleures </span><span>ventes</span></p>
        <section id='cards_container'>
            {
                products.map(element=>{
                    return(
                        <div className='card' key={element.id}>
                            <img src={element.img}/>
                            <div>
                                <p>{element.title}</p>
                                <p>{element.price} DH</p>
                            </div>
                        </div>
                    )
                })
            }
        </section>
        <a href='http://localhost:3000/toutvoir'>Tous les produits</a>
    </div>
  )
}

export default TopVentes