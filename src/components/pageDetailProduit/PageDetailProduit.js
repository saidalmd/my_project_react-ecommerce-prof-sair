import React,{useState,useEffect} from 'react';
import p1 from '../../p4.webp';
import axios from 'axios';
import './PageDetailProduit.css';
import {useParams} from 'react-router-dom';


function PageDetailProduit() {
    const [monPanier,setMonPanier]=useState(JSON.parse(localStorage.getItem('listPanier')))
    const {id}=useParams()
    const [produits,setProduits]=useState([])
    const [ajoute,setAjoute]=useState(false)

    useEffect(()=>{
        axios.get(`http://localhost:8001/produits/${id}`)
            .then(response=>setProduits(response.data))
            .catch(error=>console.log(error))
    },[])
    const styleImage={
        background:`linear-gradient(to top,rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),
        url(${p1})`,
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat", 
        backgroundSize: "cover",
        backgroundAttachment: "scroll"
    }

    const click_ajouter=()=>{
        setAjoute(true)
        // const listFromStorage=localStorage.getItem('listPanier')
        const strProduit=JSON.stringify(produits);
        const nouvelleListe=[];
        for(let element of monPanier){
            nouvelleListe.push(element)
        }
        nouvelleListe.push(strProduit)
        setMonPanier(nouvelleListe)
        localStorage.setItem('listPanier',JSON.stringify(nouvelleListe))
    }
    
  return (
    <div >
        <div id="detailProduit">
            <div>
                <img src={produits.img}/>
            </div>
            <div>
                <p>{produits.title}</p>
                <p>{produits.price} DH</p>
                <button onClick={click_ajouter}>Ajouter au panier</button>
                {ajoute && <p style={{color:"rgb(8, 48, 8)",fontSize:"1.2rem",fontFamily:"'Dancing Script', cursive"}}>* Le produit est ajouté au panier avec succés</p>}
                <p>Infos d'article</p>
                <p>Il élimine efficacement l'excès de sébum, les impuretés, le maquillage et la pollution et rafraichit la peau. Ce nettoyant à la texture gel moussante peut être utilisé sur le visage et le corps.</p>                
            </div>
        </div>
    </div>
  )
        
        }
 

export default PageDetailProduit