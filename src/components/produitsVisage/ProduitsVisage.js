import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './ProduitsVisage.css';

function ProduitsVisage() {
    const [produits,setProduits]=useState([])

    useEffect(()=>{
        axios.get("http://localhost:8001/produits")
            .then(response=>setProduits(response.data.filter(ele=>{
                return ele.type=="visage";
            })))
            .catch(error=>console.log(error))
    },[])
  return (
    <div id='produitsvisage'>
            {
                produits.map(element=>{
                    return(
                        <div className='card2' key={element.id}>
                            <img src={element.img}/>
                            <div>
                                <p>{element.title}</p>
                                <p>{element.price} DH</p>
                            </div>
                        </div>
                    )
                })
            }
    </div>
  )
}

export default ProduitsVisage